const menuproduct =[
    {
        category:"app",
        imgSrc:"assets/image/portfolio/portfolio-1.jpg"
    },
    {
        category:"card",
        imgSrc:"assets/image/portfolio/portfolio-2.jpg"

    },
    {
        category:"web",
        imgSrc:"assets/image/portfolio/portfolio-3.jpg"

    },
    {         category:"app",
       imgSrc:"assets/image/portfolio/portfolio-6.jpg"
    },
    {
        category:"card",
        imgSrc:"assets/image/portfolio/portfolio-3.jpg"

    },
    {
        category:"web",
       imgSrc:"assets/image/portfolio/portfolio-4.jpg"

    },
    {
        category:"web",
        imgSrc:"assets/image/portfolio/portfolio-7.jpg"

    },
    {
       category:"card",
        imgSrc:"assets/image/portfolio/portfolio-8.jpg"

    },
   {
        category:"card",
        imgSrc:"assets/image/portfolio/portfolio-9.jpg"

    },
];

const menuWrapper = document.querySelector(".card-img");
const allBtns = document.querySelectorAll(".btn");
const btnContainer = document.querySelector(".btn-container");

btnContainer.addEventListener("click",(e)=>{
    // const btnTarget = e.target.classList.contains("btn");
    const btnId = e.target.dataset.id;
     allBtns.forEach(btn =>{
         if(btnId){
            btn.classList.remove("active-btn");
            e.target.classList.add("active-btn");
        }
     });
});

allBtns.forEach((btn) =>{
    btn.addEventListener("click",(e)=>{
       const id = e.currentTarget.dataset.id;
        const filterMenu = menuproduct.filter((item) =>{
            return item.category == id;
        });
        if (id == "all"){
            showingProduct(menuproduct);
    }else{
        showingProduct(filterMenu);
    }

        });
});




window.addEventListener("DOMContentLoaded",()=>{
    showingProduct(menuproduct);
});

const showingProduct = (arrayProducts) =>{
   const displayProduct = arrayProducts.map((p)=>{
    return `<div class="col-lg-4 col-md-4">
    <div class="meun-wrapper">
        <div class="card gallery">
            <img class="img" src="${p.imgSrc}" alt="">
       </div>
        <div class="card-info">
            <div class="card-title">
               <h4>App 1</h4>
                <p>App</p>
           </div>
           <div class="card-icon">
           <a href="pages/portfolio-detail/portfolio-silde.html">
           <i class="fa-solid fa-plus"></i>
           </a>
                <a href="pages/portfolio-detail/portfolio-detail.html"><i class="fa-solid fa-link"></i></a>
            </div>
       </div>
    </div>
</div> 
`;
})
.join("");
menuWrapper .innerHTML = displayProduct;
}
